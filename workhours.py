#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Copyright (C) 2017,2018 datube
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# import required modules  ---------------------------------------------------

import os               # Miscellaneous operating system interfaces
import sys              # System-specific parameters and functions
from time import sleep  # Time access and conversions (sleep only)
from time import time   # Time access and conversions (sleep only)
import datetime         # Basic date and time types
import argparse         # Parser for command-line options
import re               # Regular expression operations

# Main configuration =========================================================

config = { 'version': '0.5'
  
  # Change below to your likings:
 ,'out_fmt'   : 'time_registration_week_%s.csv' # note: %s = weeknum
 ,'hm_work'   : '08:00' # 8 hours of work each day
 ,'hm_pause'  : '01:00' # easymode: 30m lunch + 2x 15m break
 ,'workdays'  : 5       # number of days of week to work

  # DOT NOT CHANGE UNLESS YOU KNOW WHAT YOU ARE DOING ------------------------
 ,'tracking'  : []
 ,'cwd'       : os.path.join(os.path.expanduser("~"),".workhours")
 ,'lock_user' : '.do_not_update'
 ,'lock_pause': '.pause_lock'
 ,'csv_cols'  : 'date,start,end,total,pause,user,label'

} #---------------------------------------------------------------------------


class time_entry(): #---------------------------------------------------------
  def __init__(self):
    self.date   = '1970-01-01'
    self.start  = '00:00'
    self.end    = '00:00'
    self.pause  = '00:00'
    self.m_pause= 0
    self.user   = '00:00'
    self.m_user = 0
    self.utype  = 'int'
    self.label  = ''
    self.total  = '00:00'
    self.m_total= 0
    self.work   = '00:00'
    self.m_work = 0

  def update(self): #---------------------------------------------------------
    (h_start,m_start)   = self.start.split(':')
    (h_end,m_end)       = self.end.split(':')

    if str(self.pause).find(':')>0:
      (h_pause, m_pause)  = str(self.pause).split(':')
      self.m_pause = int(self.hm2m("%s:%s" % (h_pause, m_pause)))
    else:
      self.m_pause = int(self.pause)
      (h_pause, m_pause) = self.m2hm(self.pause)
      self.pause = "%02d:%02d" % ( int(h_pause), int(m_pause))

    s_user = ''; h_user = 0; m_user = 0
    if len(self.user)>0:
      if self.user[0] == "-":
        s_user = '-'
      if str(self.user).isdigit() or ( self.user[0] == "-" and self.user[1:].isdigit() ):
        self.m_user = int(self.user)
        ( h_user, m_user ) = self.m2hm(int(self.user))
        self.user = "%s%02d:%02d" % (s_user, int(h_user), int(m_user))
      else:
        self.utype = 'str'
        self.m_user = int(self.hm2m(self.user))
        ( h_user, m_user ) = self.user[1:].split(':')


    m_work = (int(h_end) - int(h_start)) * 60 + (int(m_end) - int(m_start)) \
            - (int(h_pause) * 60 + int(m_pause))

    if s_user == '-':
      m_work -= (int(h_user) * 60 + int(m_user))
    else:
      m_work += (int(h_user) * 60 + int(m_user))

    self.m_work = m_work

    sign = ""
    if m_work < 0:
      m_work *= -1
      sign = "-"

    (hour, minute) = self.m2hm(m_work)
    self.work = "%s%02d:%02d" %( sign, hour, minute )

    total = (int(h_end) - int(h_start)) * 60 + (int(m_end) - int(m_start))
    (hour, minute) = self.m2hm(total)
    self.total = "%02d:%02d" %( hour, minute )


  def add_pause(self, m): #---------------------------------------------------
    self.pause = self.m_pause + m

  def m2hm(self, m): #--------------------------------------------------------
    s = ''
    if m < 0:
      m *= -1
      s= '-'
    hour    = m // 60
    m %= 60
    minute  = m
    return (hour, minute)
  
  def hm2m(self, st): #-------------------------------------------------------
    s = ''
    if st[0] == '-':
      s = '-'
      st = st[1:]
    (h,m) = st.split(':')
    return  "%s%d" % ( s, int(h) * 60 + int(m) )


def _m2hm(m): #---------------------------------------------------------------
  s = ''
  if m < 0:
    m *= -1
    s= '-'
  hour    = m // 60
  m %= 60
  minute = m
  return "%s%02d:%02d" % (s, hour, minute)


def _hm2m(st): #--------------------------------------------------------------
  s = ''
  if st[0] == '-':
    s = '-'
    st = st[1:]
  (h,m) = st.split(':')
  return  int("%s%d" % ( s, int(h) * 60 + int(m) ) )


def registration_parse(): #---------------------------------------------------

  if not os.path.exists(config['cwd']):
    os.mkdir(config['cwd'])

  if os.path.exists(config['file']):
    time_track = open(config['file'],'r').read().splitlines()
    cols = config['csv_cols'].split(',')
    if len(time_track)>0:
      line = time_track[0]
      # test if first line are column names
      if not line.find(",") > 0:
        print "Time registration file seems invalid (missing header)"
        sys.exit(0)
      else:
        if not line[0].isdigit():
          # assuming first line is column definition
          config['csv_cols'] = line
          cols = line.split(',')
          time_track = time_track[1:]
        else:
          print "Time registration file seems invalid (missing header)"
          sys.exit(0)

      for line in time_track:
        line = line.split(',')
        entry = time_entry()
        for idx in range(0,len(line)):
          if cols[idx] == 'date':
            entry.date = line[idx]
          if cols[idx] == 'start':
            entry.start = line[idx]
          if cols[idx] == 'end':
            entry.end = line[idx]
          if cols[idx] == 'pause':
            entry.pause = line[idx]
          if cols[idx] == 'user':
            entry.user = line[idx]
          if cols[idx] == 'label':
            entry.label = line[idx]
        entry.update()
        config['tracking'].append(entry)


def registration_write(): #---------------------------------------------------
  content = []
  content.append(config['csv_cols'])
  cols = config['csv_cols'].split(',')

  for entry in config['tracking']:
    line = []
    for idx in range(0,len(cols)):
      if cols[idx] == 'date':
        line.append(entry.date)
      if cols[idx] == 'start':
        line.append(entry.start)
      if cols[idx] == 'end':
        line.append(entry.end)
      if cols[idx] == 'total':
        line.append(entry.total)
      if cols[idx] == 'pause':
        line.append(entry.pause)
      if cols[idx] == 'user':
        if entry.user == '00:00':
          line.append('')
        else:
          if entry.utype == 'str':
            line.append(entry.hm2m(entry.user))
          else:
            line.append(entry.user)
      if cols[idx] == 'label':
        line.append(entry.label)

    line = ','.join(line)
    content.append(line.rstrip(','))

  # simple file locking
  lock = "%s~" % config['file']
  while os.path.exists(lock):
    sleep(.5)
  open(lock,'w').close() 
  f = open(config['file'],'w')
  f.write("\n".join(content))
  f.close()
  os.remove(lock) 


def registration_update(): #--------------------------------------------------
  entry = time_entry()
  if len(config['tracking']) > 0:
    entry = config['tracking'][-1]

  if entry.date == config['now_date']:
    entry.end = datetime.datetime.now().strftime('%H:%M')
    entry.update()
  else:
    entry = time_entry()
    entry.date = config['now_date']
    entry.start = config['now_time']
    entry.end = config['now_time']
    if config['easymode'] and config['now_dow'] < config['workdays']:
      entry.pause = config['hm_pause']
    entry.update()
    config['tracking'].append(entry)


def _update_config(): #-------------------------------------------------------
  now = datetime.datetime.now()
  config.update({'now_time': now.strftime('%H:%M') })
  config.update({'now_date': now.strftime('%Y-%m-%d') })
  config.update({'now_week': now.strftime('%V') })
  config.update({'now_dow' : now.weekday() })
  file = config['out_fmt'] % config['now_week']
  file = os.path.join(config['cwd'], file)

  config['easymode'] = False
  if 'args' in config:
    if config['args'].easy:
      config['easymode'] = True
    if 'file' in config['args']:
      if config['args'].file:
        if os.path.exists(config['args'].file):
          file = config['args'].file

  config.update({'file': file})


def toggle_user_lock(): #-----------------------------------------------------
  lock = os.path.join(config['cwd'], config['lock_user'])
  if os.path.isfile(lock):
    os.remove(lock)
    print "info: unlocked"
  else:
    open(lock,'w').close()
    print "info: locked"


def pause_lock_mode(): #------------------------------------------------------

  if _haslock():
    print "registration file locked.."
    sys.exit(1)

  lock = os.path.join(config['cwd'], config['lock_pause'])
  open(lock,'w').close()

  registration_update()
  entry = config['tracking'][-1]
  start = time()
  end   = start
  while True:
    try:
      end = time()
      sec = int(end - start)
      min = sec // 60
      sys.stdout.write("\rpause mode: %02d minute(s)" % min)
      sys.stdout.flush()
      sleep(5)
    except KeyboardInterrupt:
      sys.stdout.write("\r")
      end = time()
      sec = int(end - start)
      min = sec // 60
      entry.add_pause(min)
      entry.update()
      registration_update()
      registration_write()
      os.remove(lock)
      sys.exit()


def _haslock(): #-------------------------------------------------------------
  ret = False
  # checks for signedoff file and date..
  lock = os.path.join(config['cwd'], config['lock_user'])
  if os.path.isfile(lock):
    ret = True
 
  # are we in pause mode?
  lock = os.path.join(config['cwd'], config['lock_pause'])
  if os.path.isfile(lock):
    ret = True

  return ret


def show_totals(): #----------------------------------------------------------
  # calculate first column width
  w = 10 #  "30-01 (mo)"
  for entry in config['tracking']:
    if len(entry.label) > w:
      w = len(entry.label)

  div = "-" * ( w + 10 )
  tot_pause = 0
  tot_work  = 0
  tot_user  = {}

  # print daily totals
  for entry in config['tracking']:
    # sum pause / work time
    tot_pause += entry.m_pause
    tot_work  += entry.m_work
    mark = ''
    if entry.m_user != 0:
      mark = '*'
      # sum user times
      if entry.label in tot_user:
        m_user = tot_user[entry.label] + entry.m_user
        tot_user.update({entry.label: m_user})
      else:
        tot_user.update({entry.label: entry.m_user})
    dt = datetime.datetime.strptime(entry.date, "%Y-%m-%d")
    ds = "%s (%s)" % ( dt.strftime("%d-%m"),
                     ( dt.strftime("%A")[0:2].lower()) )
    print "%s : %su%s" % ( ds.ljust(w), entry.total, mark )
  # show pauses + user times
  print "%s+" % div
  print "%s :-%su" % ( "pause".ljust(w), _m2hm(tot_pause))

  for k in tot_user:
    if tot_user[k] != 0:
      title = k
      if title == '': title = '?'
      print "%s :%su" % ( title.ljust(w), _m2hm(tot_user[k]).rjust(6))

  # show total
  print "%s+" % div
  print "%s  %su" % (" ".ljust(w),_m2hm(tot_work).rjust(6))


def show_timer(t): #----------------------------------------------------------
  if len(config['tracking']) > 0:
    entry = config['tracking'][-1]
    countdown = _m2hm(_hm2m(config['hm_work']) - entry.m_work)
    endtime   = _m2hm(_hm2m(entry.start) + _hm2m(config['hm_work']) \
                      + entry.m_pause - entry.m_user )
    if t == 1:
      print countdown
    if t == 2:
      print endtime
    if t == 3:
      print "%s/%s" % ( countdown, endtime ) 


def show_weektimer(t): #------------------------------------------------------
  if len(config['tracking']) > 0:
    tot_week = _hm2m(config['hm_work']) * config['workdays']
    tot_work = 0
    for entry in config['tracking']:
      tot_week -= entry.m_work
      tot_work += entry.m_work

    if t == 1:
      print _m2hm(tot_week)
    if t == 2:
      print _m2hm(tot_work)
    if t == 3:
      print "%s/%s" % (_m2hm(tot_work), _m2hm(tot_week))


def show_info(): #------------------------------------------------------------
  entry = config['tracking'][-1]
  if entry.date != config['now_date']:
    print "Not much info at this moment, please update first."
  else:
    endtime   = _m2hm(_hm2m(entry.start) + _hm2m(config['hm_work']) \
                      + entry.m_pause - entry.m_user )
    print "You started at %su and spent %su on the job," % (
      entry.start, entry.work
    )
    print "took a total of %d minute(s) breaks, so you could" % (
      entry.m_pause
    )
    print "go home at %su. Have a nice day." % ( endtime )


def parse_arguments(): #------------------------------------------------------
  parser = argparse.ArgumentParser(description='Keep track of hours worked on the computer.',
                                   epilog="Without any options it just "+
                                           "updates start/end/total.")

  parser.add_argument('-e','--easy', action='store_true',
                      dest='easy',
                      help="include pauses (%su) automagically (or use -p)" % (
                          config['hm_pause']
                        )
                      )
  parser.add_argument('-f','--file', action='store',
                      dest='file',
                      help="use this file instead of default")
  parser.add_argument('-i','--info', action='store_true',
                      dest='info',
                      help="show todays info")
  parser.add_argument('-l','--lock', action='store_true',
                      dest='lock',
                      help="toggle track update lock")
  parser.add_argument('-p','--pause', action='store_true',
                      dest='pause',
                      help="start pause mode (ctrl-c to quit)")
  parser.add_argument('-s','--show', action='store_true',
                      dest='show',
                      help="show daily totals for this week")
  parser.add_argument('-t','--timer', action='count',
                      dest='timer',
                      help="today countdown timer (-tt end timer, -ttt both)")
  parser.add_argument('-T','--weektimer', action='count',
                      dest='weektimer',
                      help="week countdown timer (-TT total worked, -TTT both)")

  args = parser.parse_args()

  config.update({'args': args})


#=============================================================================
#=============================================================================
#=============================================================================

if __name__ == "__main__":
  if len(sys.argv)>1:
    parse_arguments() 

  # update config with running values
  _update_config() 

  # do what the user wants
  if not 'args' in config or config['easymode']:
    # no options (or easymode)
    if not _haslock():
      registration_parse()
      registration_update()
      registration_write()
  else:
    args=config['args']
    if args.lock:
      toggle_user_lock()
      sys.exit(0)
    registration_parse()
    if len(config['tracking']) == 0:
      print "Not much info at this moment, please update first." 
    else:
      if args.info:
        show_info()
        sys.exit(0)
      if args.show:
        show_totals()
        sys.exit(0)
      if args.timer:
        show_timer(args.timer)
      if args.weektimer:
        show_weektimer(args.weektimer)
        sys.exit(0)
      if args.pause:
        pause_lock_mode()

# vim: expandtab tabstop=2 expandtab softtabstop=2 shiftwidth=2
